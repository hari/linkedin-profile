var fs = require('fs');

function getFromFile(filename) {
  var lines = fs.readFileSync(filename).toString().split('\n');
  if (lines.length > 0) {
    var line = lines[Math.floor(Math.random() * lines.length)]
    if (line.trim().length == 0 && lines.length > 1) {
      return getFromFile(filename);
    }
    return line;
  }
  return '';
}

function login(callback) {
  var nightmare = require('nightmare')({
    show: true,
    jar: true,
    waitTimeout: 60000
  });
  var account = getFromFile('linked.txt').split(':');
  var email = account[0];
  var password = account[1];
  nightmare
    .useragent('Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0')
    //.useragent('Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30')
    .goto('https://www.linkedin.com')
    .wait('#login-email')
    .type('#login-email', email)
    .type('#login-password', password)
    .click('#login-submit')
    .wait(5000)
    .then(() => {
      callback(null, nightmare)
    })
    .catch(function (error) {
      if (error) {
        callback(error, null);
      }
    });
}

module.exports = login;