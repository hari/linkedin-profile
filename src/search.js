function search(nightmare, term, callback) {
  nightmare
    .type('[placeholder=Search]', term)
    .click('button.nav-search-button')
    .wait('[data-vertical="COMPANIES"]')
    .click('[data-vertical="COMPANIES"]')
    .wait('.search-result__result-link')
    .evaluate(() => {
      return document.querySelector('.search-result__result-link').href;
    })
    .then(link => {
      callback(null, link);
    })
    .catch(err => {
      console.log(err);
      callback(err, null);
    });
}

module.exports = search;