var builtWith = require('./src/built-with');
var searchCompany = require('./src/search');
var visitCompany = require('./src/company');
var login = require('./src/in-login');
var visitProfile = require('./src/profile');
var fs = require('fs');

function getSearchTerm(filename) {
  if (fs.exists(filename), function (yes) {
    if (!yes) {
      console.log('No file found.');
      return null;
    }
    var lines = fs.readFileSync(filename).toString().split('\n');
    if (lines.length > 0) {
      var line = lines.pop();
      if (line.trim().length == 0 && lines.length > 1) {
        return getSearchTerm(filename);
      }
      fs.writeFile(filename, lines.join("\r\n"), (err) => {
        if (err) {
          console.log(err);
        }
      });
      return line;
    }
    return null;
  });
}

var term = getSearchTerm('terms.txt');

if (term == null) {
  console.log('No terms.');
  process.exit(0);
}

function saveCompany(company) {
  fs.appendFile('company.csv', [
    company.name, company.industry, company.location, company.website, company.followers, company.employee, company.see
  ].map(item => (item == null || item.length == 0) ? item : '"' + item.replace('"', '""') + '"').join(',') + '\n', err => {
    if (err) {
      console.log(err);
    }
  });
}

function saveProfile(profile) {
  fs.appendFile('data.csv', [profile.data.name, profile.data.country,
  profile.data.company, profile.data.connectionCount
  ].join(',') + '\n', function (err) {
    if (err) {
      console.log(err);
    }
  });
}

function endInstance(nightmare) {
    nightmare.end();
    nightmare.proc.disconnect();
    nightmare.proc.kill();
    nightmare.ended = true;
    nightmare = null;
}

//Main logic
function run() {
  login((nightmare) => {
    searchCompany(nightmare, term, (link) => {
      visitCompany(nightmare, link, (company, employees) => {
        saveCompany(company);
        visitProfile(nightmare, employees, (profile) => {
          saveProfile(profile);
        }, (leftProfiles, data) => {
          if (leftProfiles.length > 0) {
            visitProfiles(nightmare, leftProfiles);
          }
        });
      });
    });
  });
}

function visitProfiles(nightmare, profiles) {
  visitProfile(nightmare, leftProfiles, (profile) => {
    saveProfile(profile);
  }, (l,d) => {
    endInstance(nightmare);
  });
}

run();